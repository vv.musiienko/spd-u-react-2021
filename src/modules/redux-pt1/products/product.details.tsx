import {useParams} from "react-router-dom";
import {useEffect} from "react";
import {Loader} from "../../../components/loader/loader";
import {useAppDispatch, useAppSelector} from "../../../store/hooks";
import {loadProduct} from "./services/actions";

export const ProductDetails = () => {
    const {id} = useParams();
    const dispatch = useAppDispatch();
    const product = useAppSelector(state => state.products.details[id!]);
    useEffect(() => {
        dispatch(loadProduct(id!));
    }, [])

    if (!product) {
        return <Loader/>
    }


    return (
        <div>
            <h2>Product details</h2>
            <div>{product.title}</div>
            <div>{product.description}</div>
            <div>{product.category}</div>
            <img src={product.image} alt={product.title}/>
        </div>
    )
}