import {useEffect, useRef} from 'react';


export function useLogger<T extends Object>(name: string, props: T) {
    const prevProps = useRef<T>(props);

    useEffect(() => {
        Object.keys(props).forEach(key => {
            // @ts-ignore
            if (prevProps.current[key] !== props[key]) {
                // @ts-ignore
                console.log(`${name}: %c${key}`, 'color: red', `changed from ${prevProps.current[key]} to ${props[key]}`);
            }

        })
        prevProps.current = props;
    }, [props]);

    return null;
}