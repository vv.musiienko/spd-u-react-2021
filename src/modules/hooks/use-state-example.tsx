import {useState} from "react";

export const UseStateExample = () => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [count, setCount] = useState(0);

    return <div>{count}</div>;
}