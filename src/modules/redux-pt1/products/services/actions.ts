import {createAction, createAsyncThunk} from "@reduxjs/toolkit";
import {AppDispatch} from "../../../../store/typedef";
import {Product} from "../typedef";


export const loadProductsAction = createAction<Product[]>('loadProducts');
export const loadProducts = () => async (dispatch: AppDispatch) => {
    try {
        const result = await fetch('https://fakestoreapi.com/products')
            .then(res => res.json());
        if (result) {
            dispatch(loadProductsAction(result));
        }
    } catch (e) {
    }
}

// preferred
export const loadProductsThunk = createAsyncThunk<Product[]>('load-products', async () => {
    return await fetch('https://fakestoreapi.com/products')
        .then(res => res.json());
});



export const loadProduct = createAsyncThunk<Product, string>('load-product', async (id) => {
    return await fetch(`https://fakestoreapi.com/products/${id}`)
        .then(res => res.json())
})


// loadProductsThunk.pending
// loadProductsThunk.fulfilled
// loadProductsThunk.rejected