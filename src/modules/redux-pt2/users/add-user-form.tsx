import {useAppDispatch} from "../../../store/hooks";
import {createUser} from "./services/actions-creators";
import {UserForm} from "./user-form";
import {UserPayload} from "../typedef";

export const AddUserForm = () => {
    const dispatch = useAppDispatch();

    const handleSubmit = ({userName, firstName, lastName, role}: UserPayload) => {
        return dispatch(createUser({userName, firstName, lastName, role}));
    };

    return (
        <UserForm caption="Add new user" onSubmit={handleSubmit}/>
    )
}

