import {createSelector, ParametricSelector, Selector} from "@reduxjs/toolkit";
import {AppState} from "../../../../store/typedef";
import {User} from "../../typedef";

// @ts-ignore
export const selectUsers: Selector<AppState, User[]> = state => state.users;
export const selectUserId: ParametricSelector<AppState, string, string> = (state, props) => props;


export const selectUser = createSelector(
    selectUsers,
    selectUserId,
    (users, userId) => {
        return users.find(user => user._id === userId) as User;
    }
);


