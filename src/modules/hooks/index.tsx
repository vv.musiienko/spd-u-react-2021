import React from 'react';
import {Navigate, Route, Routes} from 'react-router-dom';
import Countdown from './components/countdown/class-to-hooks-example';
import {CountdownHooks} from './components/countdown/class-to-hooks-example-modified';
import {UseCallbackExample} from './use-callback-example';
import {HeavyCalc} from './use-memo-example';
import {PageSizeUsage} from './page-size-usage';
import {ToggleExample} from "./toggle";
import {UseEffectExample} from "./use-effect-example";
import {UseFilterExample} from "./use-filter-example";
import {UseUserExample} from "./use-user-example";
import {UseRefExample} from "./use-ref-example";
import {UseStateExample} from "./use-state-example";

const exampleDataSet = new Array(1000)
    .fill(null)
    .map((_, idx) => {
        return {
            id: idx,
            caption: `Item: ${idx}`
        }
    });

export const Hooks = () => {
    const date = +new Date('2022');

    return (
        <Routes>
            <Route path="/">
                <Route path="use-effect" element={<UseEffectExample/>}/>
                <Route path="countdown-example" element={<Countdown endDate={date}/>}/>
                <Route path="countdown-hooks-example" element={<CountdownHooks endDate={date}/>}/>
                <Route path="use-callback-example" element={<UseCallbackExample/>}/>
                <Route path="use-memo" element={<HeavyCalc/>}/>
                <Route path="use-pagination-example" element={<PageSizeUsage data={exampleDataSet}/>}/>
                <Route path="use-filter-example" element={<UseFilterExample/>}/>
                <Route path="toggle-example" element={<ToggleExample/>}/>
                <Route path="use-user" element={<UseUserExample/>}/>
                <Route path="use-ref" element={<UseRefExample/>}/>
                <Route path="use-state" element={<UseStateExample/>}/>
                <Route index element={<Navigate to="use-state"/>}/>
            </Route>
        </Routes>
    )
};
