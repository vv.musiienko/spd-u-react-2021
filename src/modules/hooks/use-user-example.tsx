import {useUser} from './hooks/use-user';

export const UseUserExample = () => {
    const user = useUser('1');
    return (
        <pre>
            {JSON.stringify(user, null, 4)}
        </pre>
    )
}