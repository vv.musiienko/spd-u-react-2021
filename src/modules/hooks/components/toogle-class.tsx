import {Component, ReactNode} from "react";

interface Props {
    anchor: ReactNode,
    children: ReactNode,
}

interface State {
    shown: boolean;
}

export class ToggleTextClass extends Component<Props, State> {
    state = {
        shown: false
    };

    toggle = () => {
        this.setState(({shown}) => ({shown: !shown}));
    }

    render() {
        return (
            <div>
                <div onClick={this.toggle}>{this.props.anchor}</div>
                {this.state.shown ? this.props.children : null}
            </div>
        )
    }
}

