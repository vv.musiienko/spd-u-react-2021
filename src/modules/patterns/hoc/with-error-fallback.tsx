import React, {Component, ComponentType} from 'react';

type State = {
    hasError: boolean
};

export function withErrorFallback<T>(WrappedComponent: ComponentType<T>, name: string) {
    return class extends Component<T, State> {
        state: State = {
            hasError: false
        };

        static getDerivedStateFromError() {
            return {hasError: true};
        }

        componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
            console.error(`Error caught from ${name}`, error);
        }

        render() {
            if (this.state.hasError) {
                return <div>Something went wrong.</div>;
            }

            return <WrappedComponent {...this.props as T}/>;
        }
    }
}
