import {useAppDispatch, useAppSelector} from "../../../store/hooks";
import {AddUserForm} from "./add-user-form";
import {deleteUser, loadUsers, clearAll} from "./services/actions-creators";
import {useEffect} from "react";
import {Button, Table} from "react-bootstrap";
import {EditUserForm} from "./edit-user-form";
import {UserRoles} from "./constants";
import {userSelectors} from "./services/users.slice";

const USER_ROLE_TO_CAPTION = {
    [UserRoles.VIEWER]: 'Viewer',
    [UserRoles.EDITOR]: 'Editor',
    [UserRoles.ADMIN]: 'Admin'
};

export const Users = () => {
    const users = useAppSelector(userSelectors.selectAll);
    // const users = useAppSelector(state => state.users);
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(loadUsers());
    }, []);

    const removeAll = () => {
        dispatch(clearAll());
    }

    const onDelete = (userId: string) => () => {
        dispatch(deleteUser(userId));
    }

    return (
        <>
            <Button onClick={removeAll}>Clear all</Button>
            <AddUserForm/>
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th>Username</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Role</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                {users.map(user => (
                    <tr key={user._id}>
                        <td>{user.userName}</td>
                        <td>{user.firstName}</td>
                        <td>{user.lastName}</td>
                        <td>{USER_ROLE_TO_CAPTION[user.role]}</td>
                        <td>
                            <Button className="m-2" onClick={onDelete(user._id)}>Delete</Button>
                            <EditUserForm userId={user._id}/>
                        </td>
                    </tr>
                ))}
                </tbody>
            </Table>
        </>
    )
}