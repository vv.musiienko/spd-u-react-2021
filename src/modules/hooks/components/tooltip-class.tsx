import {Component, ReactNode} from "react";

interface Props {
    anchor: ReactNode,
    children: ReactNode,
}

interface State {
    shown: boolean;
}

export class TooltipClass extends Component<Props, State> {
    state = {
        shown: false
    };

    show = () => {
        this.setState({shown: true});
    }

    hide = () => {
        this.setState({shown: false});
    }

    render() {
        return (
            <div onMouseEnter={this.show} onMouseLeave={this.hide}>
                {this.props.anchor}
                {this.state.shown ?
                    <div style={{border: '1px solid black', background: '#FFF'}}>{this.props.children}</div> : null}
            </div>
        )
    }
}

