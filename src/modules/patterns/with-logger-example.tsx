import {withLogger} from "./hoc/with-logger";
import {withErrorFallback} from "./hoc/with-error-fallback";

interface Props {
    count: number
}

export const WithLoggerExample = ({count}: Props) => {
    // @ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    // count.test.test

    return (
        <div>
            Wrappped component {count}
        </div>
    )
};


export default withErrorFallback(withLogger(WithLoggerExample), 'hoc-example');
