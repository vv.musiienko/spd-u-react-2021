import {createContext} from "react";
import {User} from "../../typedef";

type AppContextType = {
    user: User
}

export const AppContext = createContext<AppContextType>({} as AppContextType);