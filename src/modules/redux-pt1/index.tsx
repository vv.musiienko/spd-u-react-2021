import {Route, Routes} from "react-router-dom";
import {Products} from "./products/products";
import {ProductDetails} from "./products/product.details";
import React from "react";

export const ReduxPt1 = () => {
    return (
        <Routes>
            <Route>
                <Route index element={<Products/>}/>
                <Route path=":id" element={<ProductDetails/>}/>
            </Route>
        </Routes>);
}