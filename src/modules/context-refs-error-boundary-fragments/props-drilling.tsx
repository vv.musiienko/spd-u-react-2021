import React, {FC} from "react";
import {User} from "../../typedef";
import {ErrorBoundary} from "./error-boundary";

// DON'T WRITE CODE IN A SINGLE FILE, IT'S JUST AN EXAMPLE

interface Props {
    user: User
}

export const PropsDrilling: FC<Props> = ({user}) => {
    return (
        <ErrorBoundary moduleName="props-drilling">
            <Navigation user={user}/>
        </ErrorBoundary>
    );
};


type NavigationProps = {
    user: User
}

const Navigation = (props: NavigationProps) => {
    return (
        <div>
            Navigation here
            <UserProfile user={props.user}/>
        </div>
    )

};


type UserProfileProps = {
    user: User
};

const UserProfile: FC<UserProfileProps> = (props) => (
    <div>
        {/*{props.user.name.firstname} {props.user.name.lastname}*/}
        User profile
    </div>
);
