import {Products} from './products';
import PaginatedView from './products-with-pagination';

export const ProductsView = () => (
    <Products>
        {products => <PaginatedView data={products}/>}
    </Products>
);
