import {Route, Routes} from "react-router-dom";
import React from "react";
import {Users} from "./users/users";

export const ReduxPt2 = () => {
    return (
        <Routes>
            <Route path="/users">
                <Route index element={<Users/>}/>
            </Route>
        </Routes>);
}