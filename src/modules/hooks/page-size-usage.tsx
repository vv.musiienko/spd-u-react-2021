import React, {FC} from 'react';
import {usePagination} from "./usePagination";

type Data = {
    caption: string,
    id: number
}

type Props = {
    data: Data[]
};


export const PageSizeUsage: FC<Props> = props => {
    const {pageData} = usePagination<Data>(props.data, 10);

    return (
        <div>
            <ul>
                {pageData && pageData.map(item => (<li key={item.id}>{item.caption}</li>))}
            </ul>
        </div>
    )
};
