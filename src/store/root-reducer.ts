import {combineReducers} from "@reduxjs/toolkit";
import {products} from "../modules/redux-pt1/products/services/reducers";
import { users } from "../modules/redux-pt2/users/services/users.slice";

export const rootReducer = combineReducers({
    products,
    users: users.reducer
})