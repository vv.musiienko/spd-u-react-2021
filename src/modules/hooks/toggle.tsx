import {ToggleText} from './components/toggle';
import {Tooltip} from './components/tooltip';
import {TooltipClass} from "./components/tooltip-class";
import {ToggleTextClass} from "./components/toogle-class";

export const ToggleExample = () => {
    return (
        <div>
            <ToggleText anchor={<p>Click me!</p>}>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquid animi asperiores aut
                    autem
                    beatae consectetur doloribus eius enim eos explicabo, harum id numquam possimus quas ratione
                    repellendus
                    vero vitae!
                </p>
            </ToggleText>

            <ToggleTextClass anchor={<p>Click me!</p>}>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquid animi asperiores aut
                    autem
                    beatae consectetur doloribus eius enim eos explicabo, harum id numquam possimus quas ratione
                    repellendus
                    vero vitae!
                </p>
            </ToggleTextClass>

            <Tooltip anchor={<p>test tooltip</p>}>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquid animi asperiores aut
                    autem
                    beatae consectetur doloribus eius enim eos explicabo, harum id numquam possimus quas ratione
                    repellendus
                    vero vitae!
                </p>
            </Tooltip>

            <TooltipClass anchor={<p>test tooltip</p>}>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquid animi asperiores aut
                    autem
                    beatae consectetur doloribus eius enim eos explicabo, harum id numquam possimus quas ratione
                    repellendus
                    vero vitae!
                </p>
            </TooltipClass>
        </div>
    )
}