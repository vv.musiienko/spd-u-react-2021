import React, {ComponentType, useEffect, useState} from 'react';

export interface WithUserRoleProps {
    userRole: 'admin' | 'user' | 'unauthorized'
}
export function withUserRole<T extends WithUserRoleProps>(WrappedComponent: ComponentType<T>) {
    type Props = Omit<T, keyof WithUserRoleProps>

    return (props: Props) => {
        const [userRole, setUserRole] = useState('unauthorized');

        useEffect(() => {
            setUserRole(fakeServer.getUserRole());
        }, []);

        return <WrappedComponent {...props as T} userRole={userRole}/>
    }
}

const fakeServer = {
    getUserRole: () => {
        return 'admin'
    }
};
