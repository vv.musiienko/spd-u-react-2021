import {useState} from "react";

export const StateFunctional = () => {
    const [modified, setModified] = useState(false);
    const onClick = () => setModified(true);

    return (
        <div>
            <Child onClick={onClick}/>
            {modified ? 'modified' : null}
        </div>
    )
}


interface Props {
    onClick: () => void
}

const Child = ({onClick}: Props) => (
    <button onClick={onClick}>Modify parent state</button>
)
