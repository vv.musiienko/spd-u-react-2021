// w/o immer
import {createReducer} from "@reduxjs/toolkit";
import {User} from "../../typedef";
import {clearAll, createUser, deleteUser, editUser, loadUsers} from "./actions-creators";


// without immer
export const users2 = createReducer([] as User[], builder => builder
    .addCase(loadUsers.fulfilled, (state, action) => {
        return action.payload;
    })
    .addCase(createUser.fulfilled, (state, action) => {
        return [...state, action.payload];
    })
    .addCase(deleteUser.fulfilled, (state, action) => {
        return state.filter(user => user._id !== action.meta.arg);
    })
    .addCase(editUser.fulfilled, (state, action) => {
        return state.map(user => {
            if (user._id === action.meta.arg.userId) {
                return {
                    ...user,
                    ...action.meta.arg.user
                }
            }
            return user
        })
    })
    .addCase(clearAll, () => [] as User[])
);


// immer

export const users = createReducer([] as User[], builder => builder
    .addCase(loadUsers.fulfilled, (state, action) => {
        return action.payload;
    })
    .addCase(createUser.fulfilled, (state, action) => {
        state.push(action.payload);
    })
    .addCase(deleteUser.fulfilled, (state, action) => {
        state.splice(state.findIndex(user => user._id === action.meta.arg));
    })
    .addCase(editUser.fulfilled, (state, action) => {
        const idx = state.findIndex(user => user._id === action.meta.arg.userId);
        state[idx] = {...state[idx], ...action.meta.arg.user};
    })
    .addCase(clearAll, () => [])
)


