import React from 'react';
import {useEffect, useRef, useState} from 'react';
import './countdown.css';

type Props = {
    endDate: number
}

type TimeLeft = {
    days: number,
    hours: number,
    minutes: number,
    seconds: number
};

const REFRESH_INTERVAL = 1000;

const getRemainingTime = (startDate: number) => {
    const seconds = Math.floor((startDate / 1000) % 60);
    const minutes = Math.floor((startDate / 1000 / 60) % 60);
    const hours = Math.floor((startDate / (1000 * 60 * 60)) % 24);
    const days = Math.floor(startDate / (1000 * 60 * 60 * 24));

    return {days, hours, minutes, seconds};
};

const buildElement = (caption: string, value: number) => (
    <div className="countdown__item">
        <div className="countdown__value">{value}</div>
        <div className="countdown__caption">{caption}</div>
    </div>);


export const CountdownHooks = (props: Props) => {
    const [timeLeft, setTimeLeft] = useState<TimeLeft>({
            days: 0,
            hours: 0,
            minutes: 0,
            seconds: 0
        }
    );

    const intervalId = useRef(0);

    useEffect(() => {
        const tick = () => {
            const newValue = +props.endDate - +new Date();
            setTimeLeft(getRemainingTime(newValue));
        };

        intervalId.current = window.setInterval(tick, REFRESH_INTERVAL);
        tick();

        return () => window.clearInterval(intervalId.current);
    }, [props.endDate]);

    const {days, hours, minutes, seconds} = timeLeft;

    return (
        <div className="countdown">
            <div className="countdown__container">
                {buildElement('Days', days)}
                {buildElement('Hours', hours)}
                {buildElement('Minutes', minutes)}
                {buildElement('Seconds', seconds)}
            </div>
        </div>
    );
};
