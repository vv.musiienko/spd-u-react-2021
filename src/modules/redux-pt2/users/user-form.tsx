import {Button, Form, Modal} from "react-bootstrap"
import {ChangeEvent, FormEvent, useState} from "react";
import {UserRoles} from "./constants";
import {UserPayload} from "../typedef";

interface Props {
    onSubmit: (user: UserPayload) => Promise<any>,
    caption: string,
    userName: string,
    firstName: string,
    lastName: string,
    role: UserRoles
}

export const UserForm = (props: Props) => {
    const [userName, setUserName] = useState(props.userName);
    const [firstName, setFirstName] = useState(props.firstName);
    const [lastName, setLastName] = useState(props.lastName);
    const [role, setRole] = useState(props.role);
    const [shown, setShown] = useState(false);

    const onUserNameChange = (event: ChangeEvent<HTMLInputElement>) => {
        setUserName(event.target.value);
    }
    const onFirstNameChange = (event: ChangeEvent<HTMLInputElement>) => {
        setFirstName(event.target.value);
    }
    const onLastNameChange = (event: ChangeEvent<HTMLInputElement>) => {
        setLastName(event.target.value);
    }

    const onRoleChange = (event: ChangeEvent<HTMLSelectElement>) => {
        setRole(event.target.value as unknown as UserRoles);
    }

    const openModal = () => {
        setShown(true);
    }

    const closeModal = () => {
        setShown(false);
    }

    const handleSubmit = async (e: FormEvent) => {
        e.preventDefault();
        await props.onSubmit({
            userName,
            firstName,
            lastName,
            role
        });

        closeModal();
    }

    return (
        <>
            <Button className="m-2" onClick={openModal}>{props.caption}</Button>
            <Modal show={shown} onHide={closeModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Create User</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={handleSubmit}>
                        <Form.Group className="mb-3" controlId="userName">
                            <Form.Label>Username</Form.Label>
                            <Form.Control type="text" placeholder="Username" onChange={onUserNameChange}
                                          value={userName}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="firsName">
                            <Form.Label>First name</Form.Label>
                            <Form.Control type="text" placeholder="First name" onChange={onFirstNameChange}
                                          value={firstName}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="lastName">
                            <Form.Label>Last name</Form.Label>
                            <Form.Control type="text" placeholder="Last name" onChange={onLastNameChange}
                                          value={lastName}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="role">
                            <Form.Label>Role</Form.Label>
                            <Form.Select className="me-sm-2" onChange={onRoleChange} value={role}>
                                <option value={UserRoles.VIEWER}>Viewer</option>
                                <option value={UserRoles.EDITOR}>Editor</option>
                                <option value={UserRoles.ADMIN}>Admin</option>
                            </Form.Select>
                        </Form.Group>
                        <Button type="submit">Submit form</Button>
                    </Form>
                </Modal.Body>
            </Modal>
        </>
    )
}

UserForm.defaultProps = {
    userName: '',
    firstName: '',
    lastName: '',
    role: UserRoles.VIEWER
}
