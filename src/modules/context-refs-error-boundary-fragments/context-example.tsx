import {Component, useContext} from "react";
import {AppContext} from "./context";

export const ContextExample = () => {
    const {user} = useContext(AppContext);

    return (
        <pre>{JSON.stringify(user, null, 4)}</pre>
    );
}


export const ContextExampleConsumer = () => {
    return (

        <AppContext.Consumer>
            {({user}) => (<pre>{JSON.stringify(user, null, 4)}</pre>)}
        </AppContext.Consumer>
    );
}

export class ContextExampleClass extends Component {
    static contextType = AppContext;

    render() {
        return (<div>{this.context.user.name.firstname}</div>)
    }
}
