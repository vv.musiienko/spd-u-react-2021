import React, {lazy, Suspense} from 'react';
import './App.scss';
import {
    Route,
    Routes,
    BrowserRouter
} from "react-router-dom";
import {Hooks} from './modules/hooks';
import {Layout} from "./layout";
import {ContextRefs} from "./modules/context-refs-error-boundary-fragments";
import {Loader} from "./components/loader/loader";
import {Patterns} from "./modules/patterns";
import {ReduxPt1} from "./modules/redux-pt1";
import {Provider} from "react-redux";
import {store} from "./store/store";
import {ReduxPt2} from './modules/redux-pt2';

const RoutingCodeSplitting = lazy(() => import('./modules/routing-code-splitting'));

function App() {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Layout/>}>
                        <Route path="hooks/*" element={<Hooks/>}/>
                        <Route path="context-refs-boundary/*" element={<ContextRefs/>}/>
                        <Route path="routing-code-splitting/*" element={
                            <Suspense fallback={<Loader/>}>
                                <RoutingCodeSplitting/>
                            </Suspense>
                        }/>
                        <Route path="patterns/*" element={<Patterns/>}/>
                        <Route path="redux-pt1/*" element={<ReduxPt1/>}/>
                        <Route path="redux-pt2/*" element={<ReduxPt2/>}/>
                    </Route>
                    <Route path="*" element={<div>:(</div>}/>
                </Routes>
            </BrowserRouter>
        </Provider>
    );
}

export default App;
