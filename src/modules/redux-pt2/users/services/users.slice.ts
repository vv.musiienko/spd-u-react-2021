import {
    createEntityAdapter,
    createSlice,
} from "@reduxjs/toolkit";
import {User} from "../../typedef";
import {AppState} from "../../../../store/typedef";
import {createUser, deleteUser, editUser, loadUsers} from "./actions-creators";


const usersAdapter = createEntityAdapter<User>({
    selectId: user => user._id
});

export const userSelectors = usersAdapter.getSelectors<AppState>(state => state.users);

export const users = createSlice({
    name: 'users',
    initialState: usersAdapter.getInitialState(),
    reducers: {
        clearAll: () => usersAdapter.getInitialState()
    },
    extraReducers: builder => builder
        .addCase(createUser.fulfilled, (state, action) => {
            usersAdapter.addOne(state, action.payload);
        })
        .addCase(loadUsers.fulfilled, usersAdapter.setAll)
        .addCase(deleteUser.fulfilled, (state, action) => {
            usersAdapter.removeOne(state, action.meta.arg)
        })
        .addCase(editUser.fulfilled, (state, action) => {
            usersAdapter.updateOne(state, {id: action.meta.arg.userId, changes: action.meta.arg.user});
        })
});

export const {clearAll} = users.actions;