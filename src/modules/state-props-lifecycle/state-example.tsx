import React, {Component} from 'react';
import clsx from "clsx";

type Props = {
    type: 'PRIMARY' | 'SECONDARY',
    caption: string,
    onClick: () => void
}

interface State {
    busy: boolean,
    modified: boolean
}

export class StateExample extends Component<Props, State> {
    static defaultProps = {
        onClick: () => {}
    };

    state = {
        busy: false,
        modified: false
    };

    onClick = () => {
        this.setState({busy: true, modified: false});
        this.props.onClick();
    };

    onFocus = () => {

    }


    render() {
        const classes = clsx('button', {
            'button_primary': this.props.type === 'PRIMARY',
            'button_secondary': this.props.type === 'SECONDARY'
        });

        return (
            <button className={classes} onClick={this.onClick} disabled={this.state.busy} onFocus={this.onFocus}>
                {this.props.caption}
                {this.state.busy && <span>...</span>}
            </button>
        );
    }
}
