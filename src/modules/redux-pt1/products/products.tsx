import {useEffect} from "react";
import {NavLink, Outlet} from "react-router-dom";
import {useAppDispatch, useAppSelector} from "../../../store/hooks";
import {loadProductsThunk} from "./services/actions";

export const Products = () => {
    const dispatch = useAppDispatch();
    const products = useAppSelector(state => state.products.all);

    useEffect(() => {
        if (!products.length) {
            dispatch(loadProductsThunk());
        }

    }, [])

    return (
        <div>
            <div>Products list</div>
            <ul>
                {products.map(product => (
                    <li key={product.id}>
                        <NavLink to={String(product.id)}>
                            {product.title} - {product.category} - ${product.price}
                        </NavLink>
                    </li>
                ))}
            </ul>
            <Outlet/>
        </div>
    )
}

