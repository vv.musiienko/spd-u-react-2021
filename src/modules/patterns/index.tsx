import React, {useState} from 'react';
import {Route, Routes} from 'react-router-dom';
import HocExample from "./with-logger-example";
import WithUserRoleExample from "./with-user-role-example";
import {Hoc} from "./hoc";
import {ProductsList} from "./products/products-list";
import {ProductsGrid} from "./products/products-grid";
import {ProductsView} from "./products/products-custom";


export const Patterns = () => {
    const [count, setCount] = useState(0);
    const inc = () => {
        setCount(count + 1);
    }
    return (
        <Routes>
            <Route path="hoc" element={<Hoc/>}/>
            <Route path="with-logger" element={<button onClick={inc}><HocExample count={count}/></button>}/>
            <Route path="with-logger" element={<HocExample count={count}/>}/>
            <Route path="with-user-role" element={<WithUserRoleExample caption="With user role"/>}/>
            <Route path="products-list" element={<ProductsList/>}/>
            <Route path="products-grid" element={<ProductsGrid/>}/>
            <Route path="products-custom" element={<ProductsView/>}/>
        </Routes>
    )
};
