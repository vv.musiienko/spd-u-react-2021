import React, {FC} from 'react';
import {Products} from './products';
import {Card, Row} from "react-bootstrap";

type Props = {};

export const ProductsGrid: FC<Props> = () => {
    return (
        <Products>{
            products => (
                <Row>{
                    products.map(product => (
                        <Card className="m-3" style={{width: '18rem'}}>
                            <Card.Img variant="top" src={product.image}/>
                            <Card.Body>
                                <Card.Title>{product.title}</Card.Title>
                                <Card.Text>
                                    {product.description}
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    ))
                }
                </Row>
            )
        }
        </Products>
    )
};
