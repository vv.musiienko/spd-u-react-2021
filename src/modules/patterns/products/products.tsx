import React, {FC, ReactNode, useEffect, useState} from 'react';
import {Product} from '../../../typedef';

type Props = {
    children: (users: Product[]) => ReactNode
}
export const Products: FC<Props> = ({children}) => {
    const [products, setProducts] = useState<Product[] | null>(null);
    useEffect(() => {
        fetch('https://fakestoreapi.com/products')
            .then(res => res.json())
            .then(json => setProducts(json));
    }, []);

    if (!products) {
        return null;
    }
    return (
        <div>
            <h2>Products</h2>
            {children && children(products)}
        </div>
    )
};
