import {useEffect, useRef} from "react";

export const UseRefExample = () => {
    const mounted = useRef(false);

    useEffect(() => {
        mounted.current = true;
    }, []);

    return (
        <div>useRef</div>
    );
}