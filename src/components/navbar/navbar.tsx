import React, {FC, ReactNode} from 'react';

type Props = {
    children: ReactNode,
};

export const Navbar: FC<Props> = props => {
    return (
        <nav className="navbar navbar-expand navbar-light bg-light mb-3">
            {props.children}
        </nav>
    )
};
