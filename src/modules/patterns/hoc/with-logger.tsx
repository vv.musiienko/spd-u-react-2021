import {ComponentType, useEffect, useRef} from "react";

export function withLogger<T extends Object>(WrappedComponent: ComponentType<T>) {
    const Wrapper = (props: T) => {
        const prevProps = useRef<T>(props);

        useEffect(() => {
            (Object.keys(props) as Array<keyof T>).forEach(key => {
                if (prevProps.current[key] !== props[key]) {
                    console.log(`${WrappedComponent.name}: %c${key}`, 'color: red', `changed from ${prevProps.current[key]} to ${props[key]}`);
                }
            })
            prevProps.current = props;
        }, [props]);


        return <WrappedComponent {...props}/>
    }

    Wrapper.displayName = `withLogger(${WrappedComponent.name})`;

    return Wrapper;
}