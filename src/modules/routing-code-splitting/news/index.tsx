import React, {ChangeEvent, KeyboardEvent, FC, useEffect, useState} from 'react';
import {useSearchParams} from 'react-router-dom';
import {NewsPreview} from './news-preview';
import {NewsItem} from './typedef';
import {Loader} from "../../../components/loader/loader";

export const News: FC<{}> = () => {
    const [searchParams, setSearchParams] = useSearchParams();
    const [news, setNews] = useState<NewsItem[]>([]);
    const [loading, setLoading] = useState(false);
    const [query, setQuery] = useState<string>(searchParams.get('tag') || '');

    useEffect(() => {
        const searchQuery = searchParams.get('tag');
        if (!searchQuery) {
            return;
        }
        setLoading(true);
        fetch(`https://newsapi.org/v2/everything?q=${searchQuery}&sortBy=publishedAt&pageSize=30&apiKey=${process.env.REACT_APP_API_KEY}`)
            .then(response => response.json())
            .then(data => {
                setLoading(false);
                setNews(data.articles);
            });
    }, [searchParams]);


    const applySearchParams = () => {
        setSearchParams({
            tag: query
        });
    }

    const onKeyDown = (e: KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            applySearchParams();
        }
    }


    const onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        setQuery(e.target.value)
    }

    return (
        <div className="news">
            <h2>News</h2>
            <input
                className="form-control"
                type="text"
                onChange={onInputChange}
                onBlur={applySearchParams}
                value={query}
                onKeyDown={onKeyDown}
            />
            {loading ? <Loader/> : null}
            {
                news.length !== 0 && news.map((newsItem, idx) => {
                    return <NewsPreview data={newsItem} key={idx}/>
                })
            }
            {!loading && news.length === 0 && <div>There are no articles for {query} tag</div>}
        </div>
    )
};

export default News;
