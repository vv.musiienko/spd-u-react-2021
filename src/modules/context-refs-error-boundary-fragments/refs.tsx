import React, {ChangeEvent, Component, createRef, useRef} from 'react';

type State = {
    value: string
};

export class RefsExample extends Component<{}, State> {
    state = {
        value: ''
    };

    input = createRef<HTMLInputElement>();

    onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        this.setState({value: e.target.value});
    };

    onFocus = () => {
        if (this.input.current) {
            console.log(this.input.current);
            this.input.current.focus();
        }
    };

    render() {
        return (
            <div>
                value: {this.state.value}
                <div>
                    <button onClick={this.onFocus}>Set input focus</button>
                    <div style={{height: '1300px'}}/>
                    <input ref={this.input} value={this.state.value} onChange={this.onInputChange}/>
                </div>
            </div>
        )
    }
}


export const RefExampleFn = () => {
    const inputRef = useRef<HTMLInputElement>(null);


    const onFocus = () => {
        if(inputRef.current) {
            inputRef.current.focus();
        }
    }

    return (
        <div>
            <div>
                <button onClick={onFocus}>Set input focus</button>
                <div style={{height: '1300px'}}/>
                <input ref={inputRef}/>
            </div>
        </div>
    )
}
