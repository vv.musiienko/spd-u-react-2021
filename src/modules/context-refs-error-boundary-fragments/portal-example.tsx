import './portal-example.scss';
import {Portal} from "./portal";

export const PortalExample = () => {

    return (
        <div className="portal-example">
            <Portal>
                <div className="portal-example-tooltip">
                    content
                </div>
            </Portal>
        </div>
    )
}