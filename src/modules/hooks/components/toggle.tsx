import {FC, ReactNode} from 'react';
import {useShown} from '../hooks/use-shown';

type Props = {
    anchor: ReactNode,
    children: ReactNode
}

export const ToggleText: FC<Props> = ({anchor, children}) => {
   const {shown, toggle, show} = useShown();

    return (
        <div>
            <div onMouseEnter={show} onClick={toggle}>{anchor}</div>
            {shown ? children : null}
        </div>
    )
}