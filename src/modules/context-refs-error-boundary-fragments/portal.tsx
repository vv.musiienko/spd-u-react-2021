import ReactDOM from 'react-dom';
import {ReactNode} from 'react';

interface Props {
    children: ReactNode
}

export const Portal = (props: Props) => ReactDOM.createPortal(
    props.children,
    document.querySelector('.popup-container') as HTMLDivElement
);
