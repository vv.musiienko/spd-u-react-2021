export type User = {
    id: number,
    email: string,
    password: string,
    name: {
        firstname: string,
        lastname: string
    },
    phone: string,
    address: Address,
    __v: number
}

type Address = {
    geolocation: {
        lat: string,
        long: string
    },
    city: string,
    street: string,
    number: number,
    zipcode: string,
}

export type Product = {
    id: number,
    title: string,
    price: number,
    description: string,
    category: string,
    rating: {
        rate: number,
        count: number
    },
    image: string
}
