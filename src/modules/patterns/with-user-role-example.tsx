import {withUserRole, WithUserRoleProps} from "./hoc/with-user-role";

interface Props extends WithUserRoleProps {
    caption: string
}

export const WithUserRoleExample = ({caption, userRole}: Props) => {
    return (
        <div>
            {caption} - {userRole}
        </div>
    )
}

export default withUserRole(WithUserRoleExample);
