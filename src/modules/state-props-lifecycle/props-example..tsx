import React, {Component} from 'react';
import {usePagination} from "../hooks/usePagination";

interface Props {
    data: string[]
}

export class PropsExample extends Component<Props, any> {
    static defaultProps = {
        data: []
    }
    render() {
        if (!this.props.data.length) {
            return <div>No data to display</div>;
        }

        return (
            <>
                {this.props.data.map(item => <div key={item}>{item}</div>)}
            </>
        )
    }
}



export const PropsExampleFunctional = (props: Props) => {
    const {pagination, pageData} = usePagination(props.data, 10);
    if (!props.data.length) {
        return <div>No data to display</div>;
    }

    return (
        <>
            <ul>
                {pageData.map(item => <li key={item}>{item}</li>)}
            </ul>
            {pagination}
        </>
    );
}

PropsExampleFunctional.defaultProps = {
    data: []
}

