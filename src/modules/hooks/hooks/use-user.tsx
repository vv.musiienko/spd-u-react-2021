import {useEffect, useState} from "react";

export type User = {
    name: string,
    avatar: string
};

export const useUser = (userId: string) => {
    const [user, setUser] = useState<User>();

    useEffect(() => {
            fetch(`https://fakestoreapi.com/users/${userId}`)
                .then(result => result.json())
                .then((user) => {
                    setUser(user)
                });
        },
        [userId]
    );

    return {user};
};
