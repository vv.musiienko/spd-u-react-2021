import React, {FC} from 'react';
import {Products} from './products';
import {Card, Row} from 'react-bootstrap';

export const ProductsList: FC<{}> = () => {
    return (
        <Products>{
            (products) =>
                products.map(product => (
                    <Row>
                        <Card className="m-4">
                            <Card.Img variant="top" src={product.image} style={{width: '200px'}}/>
                            <Card.Body>
                                <Card.Title>{product.title}</Card.Title>
                                <Card.Text>
                                    {product.description}
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Row>
                ))
        }
        </Products>
    )
};
