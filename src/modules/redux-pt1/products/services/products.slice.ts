import {createSlice} from "@reduxjs/toolkit";
import {Product} from "../typedef";

type State = {
    all: Product[],
    details: {
        [id: string]: Product
    }
}

export const products = createSlice({
    name: 'products',
    initialState: {
        all: [],
        details: {}
    } as State,
    reducers: {}
})