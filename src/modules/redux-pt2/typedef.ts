import {UserRoles} from "./users/constants";

export type User = UserPayload & {
    _id: string
};


export type UserPayload = {
    userName: string,
    firstName: string,
    lastName: string,
    role: UserRoles
}