import {useEffect, useState} from "react";

export const UseEffectExample = () => {
    const [count, setCount] = useState(0);

    useEffect(() => {
        console.log('render');

        return () => {
            console.log('unmount');
        }
    }, [count]);

    return <div onClick={() => setCount(count + 1)}>Use Effect</div>;
}