import React from 'react';
import {withPagination} from '../hoc/with-pagination';
import {Product} from '../../../typedef';

type PaginatedProps = { pageData: Product[] };

const PaginatedView = (props: PaginatedProps) => {
    return (
        <div>
            {props.pageData.map(product => (
                <div key={product.id}>{product.title}</div>
            ))}
        </div>
    )
};

export default withPagination<Product>(PaginatedView);
