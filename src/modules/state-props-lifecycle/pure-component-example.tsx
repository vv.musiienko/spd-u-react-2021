import React, {PureComponent} from 'react';

type State = {
    data: string[]
}

export class PureComponentExample extends PureComponent<{}, State> {
    state = {
        data: ['a', 'b', 'c']
    };

    add = () => {
        this.state.data.push('d');
        this.setState({
            data: this.state.data
        });
    };

    render() {
        return (
            <div>
                <button onClick={this.add}>Add</button>
                <div>
                    {this.state.data.map(letter => letter)}
                </div>
            </div>
        )
    }
}
