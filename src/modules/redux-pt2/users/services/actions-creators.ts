import {createAction, createAsyncThunk} from "@reduxjs/toolkit";
import {User, UserPayload} from "../../typedef";

export const createUser = createAsyncThunk('users/create', async (user: UserPayload) => {
    return fetch('https://crudcrud.com/api/95a4c0a67e524840a7c1698c92b22415/users', {
        method: 'POST',
        body: JSON.stringify(user),
        headers: {
            'content-type': 'application/json; charset=utf-8'
        }
    }).then(result => result.json());
});

export const loadUsers = createAsyncThunk<User[]>('users/load', async () => {
    return fetch('https://crudcrud.com/api/95a4c0a67e524840a7c1698c92b22415/users')
        .then(result => result.json());
});


export const deleteUser = createAsyncThunk('users/delete', async (userId: string) => {
    return fetch(`https://crudcrud.com/api/95a4c0a67e524840a7c1698c92b22415/users/${userId}`, {
        method: 'DELETE',
        headers: {
            'content-type': 'application/json; charset=utf-8'
        }
    }).then(res => res.text());
});


interface EditUserParams {
    userId: string,
    user: UserPayload
}

export const editUser = createAsyncThunk('users/update', async (params: EditUserParams) => {
    return fetch(`https://crudcrud.com/api/95a4c0a67e524840a7c1698c92b22415/users/${params.userId}`, {
        method: 'PUT',
        body: JSON.stringify(params.user),
        headers: {
            'content-type': 'application/json; charset=utf-8'
        }
    }).then(result => result.text());
});

export const clearAll = createAction('users/clear');