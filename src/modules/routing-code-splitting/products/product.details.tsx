import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import {Product} from "./typedef";
import {Loader} from "../../../components/loader/loader";

export const ProductDetails = () => {
    const {id} = useParams();
    const [product, setProduct] = useState<Product>();

    useEffect(() => {
        fetch(`https://fakestoreapi.com/products/${id}`)
            .then(res => res.json())
            .then(json => setProduct(json))
    })

    if (!product) {
        return <Loader/>
    }


    return (
        <div>
            <h2>Product details</h2>
            <div>{product.title}</div>
            <div>{product.description}</div>
            <div>{product.category}</div>
            <img src={product.image} alt={product.title}/>
        </div>
    )
}