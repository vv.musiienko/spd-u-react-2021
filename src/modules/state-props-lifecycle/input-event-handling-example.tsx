import {ChangeEvent, Component} from 'react';

type State = {
    value: string
};

export class InputEventHandlingExample extends Component<{}, State> {
    state = {
        value: ''
    };

    onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        this.setState({value: e.target.value});
    };

    render() {
        return (
            <div>
                value: {this.state.value}
                <div>
                    <input value={this.state.value} onChange={this.onInputChange}/>
                </div>
            </div>
        )
    }
}
