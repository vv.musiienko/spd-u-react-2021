import {Container, Nav, Navbar, NavDropdown} from "react-bootstrap";
import {Outlet} from "react-router-dom";
import React from "react";

export const Layout = () => (
        <div className="container-lg">
            <Navbar bg="light" expand="lg">
                <Container>
                    <Navbar.Brand href="#home">
                        <img
                            src="https://spd-ukraine.com/wp-content/uploads/2020/08/logo_vertical_dark_blue.svg"
                            alt="SPD Ukraine"
                            height="32px"
                        />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link href="/hooks">Basics</Nav.Link>
                            <Nav.Link href="/hooks">Hooks</Nav.Link>
                            <NavDropdown title="Hooks">
                                <NavDropdown.Item href="/hooks/use-state">useState</NavDropdown.Item>
                                <NavDropdown.Item href="/hooks/use-effect">useEffect</NavDropdown.Item>
                                <NavDropdown.Item href="/hooks/use-ref">useRef</NavDropdown.Item>
                                <NavDropdown.Item href="/hooks/use-callback-example">useCallback</NavDropdown.Item>
                                <NavDropdown.Item href="/hooks/use-memo">useMemo</NavDropdown.Item>
                                <NavDropdown.Item href="/hooks/toggle-example">useShown</NavDropdown.Item>
                                <NavDropdown.Item href="/hooks/use-filter-example">useFilter</NavDropdown.Item>
                                <NavDropdown.Item href="/hooks/countdown-example">Countdown</NavDropdown.Item>
                                <NavDropdown.Item href="/hooks/countdown-hooks-example">Countdown hooks</NavDropdown.Item>
                                <NavDropdown.Item href="/hooks/use-user">useUser</NavDropdown.Item>
                            </NavDropdown>
                            <NavDropdown title="Context">
                                <NavDropdown.Item href="/context-refs-boundary/context" >Context</NavDropdown.Item>
                                <NavDropdown.Item href="/context-refs-boundary/props-drilling" >Props drilling</NavDropdown.Item>
                                <NavDropdown.Item href="/context-refs-boundary/refs" >Refs</NavDropdown.Item>
                                <NavDropdown.Item href="/context-refs-boundary/portals" >Portals</NavDropdown.Item>
                            </NavDropdown>
                            <NavDropdown title="Routing">
                                <NavDropdown.Item href="/routing-code-splitting/news">News</NavDropdown.Item>
                                <NavDropdown.Item href="/routing-code-splitting/products">Products</NavDropdown.Item>
                            </NavDropdown>
                            <NavDropdown title="Patterns">
                                <NavDropdown.Item href="/patterns/with-logger">withLogger</NavDropdown.Item>
                                <NavDropdown.Item href="/patterns/with-user-role">withUserRole</NavDropdown.Item>
                                <NavDropdown.Item href="/patterns/with-pagination">withPagination</NavDropdown.Item>
                                <NavDropdown.Item href="/patterns/products-list">Products list</NavDropdown.Item>
                                <NavDropdown.Item href="/patterns/products-grid">Products grid</NavDropdown.Item>
                                <NavDropdown.Item href="/patterns/products-custom">Products custom</NavDropdown.Item>
                            </NavDropdown>
                            <Nav.Link href="/redux-pt1">Redux</Nav.Link>
                            <Nav.Link href="/redux-pt2/users">Redux pt.2</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            <Container className="pt-3">
                <Outlet/>
            </Container>
        </div>
    )
;
