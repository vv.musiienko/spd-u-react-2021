import {createReducer} from "@reduxjs/toolkit";
import {Product} from "../typedef";
import {loadProduct, loadProductsAction, loadProductsThunk} from "./actions";

type State = {
    all: Product[],
    details: {
        [id: string]: Product
    }
}

const defaultState = {
    all: [],
    details: {}
} as State;

export const products = createReducer(defaultState, builder => builder
    .addCase(loadProductsAction, (state, action) => {
        state.all = action.payload;
    })
    .addCase(loadProductsThunk.fulfilled, (state, {payload}) => {
        state.all = payload;
    })
    .addCase(loadProduct.fulfilled, (state, {meta, payload}) => {
        state.details[meta.arg] = payload;
    })
);

