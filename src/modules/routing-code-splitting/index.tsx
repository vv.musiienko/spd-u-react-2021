import {Route, Routes} from "react-router-dom";
import News from "./news";
import { Products } from "./products/products";
import {ProductDetails} from "./products/product.details";

const RoutingCodeSplitting = () => {
    return (
        <Routes>
            <Route path="products">
                <Route index element={<Products/>}/>
                <Route path=":id" element={<ProductDetails/>}/>
            </Route>
            <Route path="news" element={<News/>}/>
        </Routes>
    )
}
export default RoutingCodeSplitting;