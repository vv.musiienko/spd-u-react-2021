import {useEffect, useState} from "react";
import {NavLink, Outlet} from "react-router-dom";
import {Product} from "./typedef";

export const Products = () => {
    const [products, setProducts] = useState<Product[]>([]);


    useEffect(() => {
        fetch('https://fakestoreapi.com/products')
            .then(res => res.json())
            .then(json => setProducts(json))
    }, [])

    return (
        <div>
            <div>Products list</div>
            <ul>
            {products.map(product => (
                <li key={product.id}>
                    <NavLink to={String(product.id)}>
                        {product.title} - {product.category} - ${product.price}
                    </NavLink>
                </li>
            ))}
            </ul>
            <Outlet/>
        </div>
    )
}