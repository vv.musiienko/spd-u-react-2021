import {generateFakeData} from '../../utils/generate-fake-data';
import {ChangeEvent, useState} from 'react';
import {useFilter} from './hooks/use-filter';

type Item = {
    id: string,
    caption: string,
    owner: string
}

const data: Item[] = generateFakeData(100, faker => ({
    id: faker.datatype.uuid(),
    caption: faker.vehicle.manufacturer(),
    owner: faker.name.findName()
}));

export const UseFilterExample = () => {
    const [query, setQuery] = useState('');
    const filteredData = useFilter(data, [
        data => data.caption.indexOf(query) !== -1
    ]);

    const changeQuery = (e: ChangeEvent<HTMLInputElement>) => {
        setQuery(e.target.value)
    }

    return (
        <div className="mt-2">
            <div className="input-group mb-2">
                <div className="input-group-prepend">
                    <span className="input-group-text">Query: </span>
                </div>
                <input className="form-control" value={query} onChange={changeQuery}/>
            </div>

            <ul className="list-group">
                {filteredData.map(item => <li className="list-group-item" key={item.id}>{item.caption}</li>)}
            </ul>
        </div>
    )
}