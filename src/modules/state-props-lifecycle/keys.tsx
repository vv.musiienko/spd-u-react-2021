import {Component, PureComponent} from "react";
import faker from 'faker';
interface State {
    names: string[]
}

export class Keys extends Component<{}, State> {
    state = {
        names: new Array(15).fill(null).map(() => faker.name.jobTitle())
    };

    removeFirst = () => {
        this.setState(({ names }) => {
            const newNames = [...names];
            newNames.splice(0, 1);

            return { names: newNames };
        });
    };

    render() {
        return (
            <>
                <ul>
                    {this.state.names.map((name) => (
                        <ListItem key={name}>{name}</ListItem>
                    ))}
                </ul>
                <button onClick={this.removeFirst}>Remove fist item</button>
            </>
        );
    }
}

class ListItem extends PureComponent {
    componentWillUnmount() {
        console.log(this.props.children);
    }
    render() {
        console.log("render");
        return <li>{this.props.children}</li>;
    }
}

