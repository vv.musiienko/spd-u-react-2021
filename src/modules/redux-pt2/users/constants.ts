export enum UserRoles {
    VIEWER,
    EDITOR,
    ADMIN
}