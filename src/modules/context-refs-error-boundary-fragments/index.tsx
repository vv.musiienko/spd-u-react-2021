import React from 'react';
import {Route, Routes} from 'react-router-dom';
import {ContextExample} from "./context-example";
import {PropsDrilling} from './props-drilling';
import {PortalExample} from "./portal-example";
import {RefsExample} from "./refs";
import {User} from "../../typedef";

export const ContextRefs = () => {
    return (
        <Routes>
            <Route path="/">
                <Route path="context" element={<ContextExample/>}/>
                <Route path="props-drilling" element={<PropsDrilling user={{} as User}/>}/>
                <Route path="portals" element={<PortalExample/>}/>
                <Route path="refs" element={<RefsExample/>}/>
            </Route>
        </Routes>
    )
};
