import {useState} from 'react';

export function useShown() {
    const [shown, setShown] = useState(false);

    const toggle = () => {
        setShown(shown => !shown);
    }

    const show = () => {
        setShown(true);
    }
    const hide = () => {
        setShown(false);
    }


    return {shown, toggle, show, hide};
}