import {useAppDispatch, useAppSelector} from "../../../store/hooks";
import {editUser} from "./services/actions-creators";
import {UserForm} from "./user-form";
import {UserPayload} from "../typedef";
import {useState} from "react";
import {selectUser} from "./services/selectors";

interface Props {
    userId: string,
}

export const EditUserForm = ({userId}: Props) => {
    const dispatch = useAppDispatch();
    const user = useAppSelector(state => selectUser(state, userId));
    // const user = useAppSelector(state => userSelectors.selectById(state, userId));

    const [count, setCount] = useState(0);

    const handleSubmit = ({userName, firstName, lastName, role}: UserPayload) => {
        return dispatch(editUser({
            userId,
            user: {userName, firstName, lastName, role}
        }));
    };

    return (
        <>
            <UserForm
                caption="Edit User"
                firstName={user?.firstName}
                lastName={user?.lastName}
                userName={user?.userName}
                role={user?.role}
                onSubmit={handleSubmit}
            />
            <button onClick={() => setCount(count + 1)}>Inc</button>
        </>
    )
}

