import React, {FC, PureComponent, useCallback, useState} from 'react';
import {generateFakeData} from "../../utils/generate-fake-data";

type Props = {};

export const UseCallbackExample: FC<Props> = () => {
    const [count, setCount] = useState(0);
    const [items, setItems] = useState(generateFakeData(40, faker => ({
        id: faker.datatype.uuid(),
        caption: faker.company.companyName()
    })));

    const onRemove = useCallback((id: string) => {
        setItems(items.filter(current => current.id !== id));
    }, [items])

    return (
        <div>
            {count}
            <button className="button" onClick={() => setCount(count + 1)}>Increment</button>
            <ul className="list-group">
                {
                    items.map(item => {
                        return <Item key={item.id} id={item.id} caption={item.caption} onRemove={onRemove}/>;
                    })
                }
            </ul>
        </div>
    )
};


type ItemProps = {
    onRemove: (id: string) => void,
    caption: string,
    id: string
};

class Item extends PureComponent<ItemProps> {
    render() {
        console.log('rerender');
        return (
            <li className="list-group-item list-group-item-action" onClick={() => this.props.onRemove(this.props.id)}>{this.props.caption}</li>
        );
    }
}
